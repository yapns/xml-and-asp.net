﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;

namespace WebApplication1
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            ds.ReadXml(Server.MapPath("Employees.xml"));
            GridView1.DataSource = ds.Tables[0].DefaultView;
            GridView1.DataBind();

            XmlDocument xmldoc = new XmlDocument();
            xmldoc.Load(Server.MapPath("Employees.xml"));
            
            XmlNodeList nodes = xmldoc.SelectNodes("/Employees/Employee[@type='Permanent']");
            foreach (XmlNode node in nodes)
            {
              ddlPermanentEmp.Items.Add(new ListItem(node.SelectSingleNode("FirstName").InnerText, node.SelectSingleNode("ID").InnerText));
            }

            nodes = xmldoc.SelectNodes("/Employees/Employee[@type='Contract']");
            foreach (XmlNode node in nodes)
            {
              ddlContractEmp.Items.Add(new ListItem(node.SelectSingleNode("FirstName").InnerText, node.SelectSingleNode("ID").InnerText));
            }

            nodes = xmldoc.SelectNodes("//Employee");
            foreach (XmlNode node in nodes)
            {
              ddlAllEmp.Items.Add(new ListItem(node.SelectSingleNode("FirstName").InnerText, node.SelectSingleNode("ID").InnerText));
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("ReadXML.aspx");
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            Response.Redirect("WriteXML.aspx");
        }


    }
}