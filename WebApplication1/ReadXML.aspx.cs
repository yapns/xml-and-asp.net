﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
              using (XmlTextReader reader = new XmlTextReader(Server.MapPath("Employees.xml")))
            {
                while (reader.Read())
               {
                 if (reader.NodeType == XmlNodeType.Text)
                 {
                     Response.Write(reader.Value);
                     Response.Write("<br>");
                 }
               }
            }
         }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
    }
}