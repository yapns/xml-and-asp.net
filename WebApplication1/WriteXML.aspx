﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WriteXML.aspx.cs" Inherits="WebApplication1.WriteXML" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 125px;
        }
        .style3
        {
            width: 239px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <b><u>Employee<br />
        </u></b><br />
        <table class="style1">
            <tr>
                <td class="style2">
                    Type:</td>
                <td class="style3">
                    <asp:DropDownList ID="DropDownList1" runat="server">
                        <asp:ListItem>Permanent</asp:ListItem>
                        <asp:ListItem>Contract</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    First Name:</td>
                <td class="style3">
                    <asp:TextBox ID="txtboxFName" runat="server" Width="205px"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    Last Name:</td>
                <td class="style3">
                    <asp:TextBox ID="txtboxLName" runat="server" Width="205px"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    ID:</td>
                <td class="style3">
                    <asp:TextBox ID="txtboxID" runat="server" Width="205px"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    Department:</td>
                <td class="style3">
                    <asp:TextBox ID="txtboxDept" runat="server" Width="205px"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
        <br />
        <asp:Button ID="btnAdd" runat="server" onclick="btnAdd_Click" Text="Add" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnUpdate" runat="server" onclick="btnUpdate_Click1" 
            Text="Update" />
        <br />
        <br />
        <br />
        <asp:Button ID="btnBackDefault" runat="server" Text="Back" 
            onclick="btnBackDefault_Click" />
    </div>
    </form>
</body>
</html>
