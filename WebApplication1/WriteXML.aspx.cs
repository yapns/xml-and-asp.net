﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace WebApplication1
{
    public partial class WriteXML : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnBackDefault_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtboxID.Text) || string.IsNullOrEmpty(txtboxFName.Text) || string.IsNullOrEmpty(txtboxLName.Text) || string.IsNullOrEmpty(txtboxDept.Text))
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), "alertmessage", "<script type='text/javascript'>alert('Please Enter Some Values');</script>");
            }
            else
            {
                XDocument document = XDocument.Load(Server.MapPath("Employees.xml"));
                document.Element("Employees").Add(new XElement("Employee", new XAttribute("type", DropDownList1.SelectedItem.Text), new XElement("ID", txtboxID.Text), new XElement("FirstName", txtboxFName.Text), new XElement("LastName", txtboxLName.Text), new XElement("Dept", txtboxDept.Text)));
                document.Save(Server.MapPath("Employees.xml"));

                ClientScript.RegisterClientScriptBlock(this.GetType(), "alertmessage", "<script type='text/javascript'>alert('Added sucessfully');</script>");
            }
        } 
        protected void btnUpdate_Click1(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtboxID.Text) || string.IsNullOrEmpty(txtboxFName.Text)|| string.IsNullOrEmpty(txtboxLName.Text) || string.IsNullOrEmpty(txtboxDept.Text))
            {
              ClientScript.RegisterClientScriptBlock(this.GetType(), "alertmessage", "<script type='text/javascript'>alert('Please Enter Some Values');</script>");
            }
            else
            {
            XDocument document = XDocument.Load(Server.MapPath("Employees.xml"));
            var updateQuery = from r in document.Descendants("Employee")
                              where r.Element("ID").Value == txtboxID.Text
                              select r;

                 foreach (var query in updateQuery)
                {
                query.Element("FirstName").SetValue(txtboxFName.Text);
                query.Element("LastName").SetValue(txtboxLName.Text);
                query.Attribute("type").SetValue(DropDownList1.SelectedItem.Text);
                query.Element("Dept").SetValue(txtboxDept.Text);
                }

            document.Save(Server.MapPath("Employees.xml"));
            ClientScript.RegisterClientScriptBlock(this.GetType(), "alertmessage", "<script type='text/javascript'>alert('Updated sucessfully');</script>");
        
            txtboxFName.Text = string.Empty;
            txtboxLName.Text = string.Empty;
            txtboxDept.Text = string.Empty;
            }
        }
    }
}