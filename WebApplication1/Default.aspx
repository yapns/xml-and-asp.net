﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            height: 23px;
        }
        .style3
        {
            height: 23px;
            width: 274px;
        }
        .style4
        {
            width: 274px;
        }
        .style5
        {
            height: 23px;
            width: 155px;
        }
        .style6
        {
            width: 155px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <b><u>Employee</u></b> <br /><br />
        <table class="style1">
            <tr>
                <td class="style5">
                    Permanent Employees:</td>
                <td class="style3">
        <asp:DropDownList ID="ddlPermanentEmp" runat="server" Height="23px" Width="265px">
        </asp:DropDownList>
                </td>
                <td class="style2">
                </td>
                <td class="style2">
                </td>
            </tr>
            <tr>
                <td class="style6">
                    Contract Employees:<br />
                </td>
                <td class="style4">
        <asp:DropDownList ID="ddlContractEmp" runat="server" Height="23px" Width="265px">
        </asp:DropDownList>
                    <br />
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>

            <tr>
                <td class="style6">
                    <br />All Employees:<br /><br />
                </td>
                <td class="style4">
       <br /> <asp:DropDownList ID="ddlAllEmp" runat="server" Height="23px" Width="265px">
        </asp:DropDownList>
                    <br /><br />
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </div>
    
    <asp:GridView ID="GridView1" runat="server">
    </asp:GridView>
    <br />
    <asp:Button ID="btnInsert" runat="server" onclick="btnInsert_Click" 
        Text="Add/ Update Employee" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="View" />
    </form>
</body>
</html>
